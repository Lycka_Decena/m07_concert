<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Concert;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfont\Bridge\Doctrine\Form\Type\EntityType;

class ConcertController extends Controller
{

	/**
	 * @Route("/insertConcert", name="insertConcert")
	 */
	public function insertConcertAction(Request $request)
	{
		$concert = new Concert();

		$form = $this->createFormBuilder($concert)
			->add('nomConcert', TextType::class)
			->add('nomGrup', TextType::class)
			->add('data', DateType::class)
			->add('ciutat', TextType::class)
			->add('espai', TextType::class)
			->add('save', SubmitType::class, array('label' => 'Create'))
			->getForm();

		$form->handleRequest($request);

		if($form->isSubmitted() && $form->isValid()){
			$em = $this->getDoctrine()->getManager();
			$em->persist($concert);
			$em->flush();

			return $this->render('default/message.html.twig',
			array('message' => 'Concert created'));
		}
		return $this->render('default/form.html.twig',
			array('title' => 'Insert Concert',
				'form' => $form->createView(),
		));
	}


	/**
	 * @Route("/selectConcert", name="selectConcert")
	 */
	public function selectConcertAction(Request $request)
	{
		$concert = new Concert();

		$form = $this->createFormBuilder($concert)
			->add('nomConcert', TextType::class)
			->add('select', SubmitType::class, array('label' => 'Select'))
			->getForm();

		$form->handleRequest($request);

		if($form->isSubmitted() && $form->isValid()){
			$concert = $this->getDoctrine()
				->getRepository('AppBundle:Concert')
				->findbyName($concert->getNomConcert());

			if(count($concert)==0){
				return $this->render('default/message.html.twig',
					array('message' => 'No concerts found'));
			}
			return $this->render('concert/content.html.twig',
				array('concert' => $concert));
		}
		return $this->render('default/form.html.twig',
			array('title' => 'Select Concert',
				'form' => $form->createView(),
		));
	}


	/**
	 * @Route("/selectAll", name="selectAll")
	 */
	public function selectAllAction()
	{
		/*$concerts = $this->getDoctrine()
			->getRepository('AppBundle:Concert')
			->findAll();*/


			print_r($concerts);

		/*if(count($concerts)==0){
			return $this->render('default/message.html.twig',
				array('message' => 'No concerts found'));
		}*/
		return $this->render('concert/content.html.twig',
			array('concerts' => $concerts));
	}

	/**
	 * @Route("/deleteConcert", name="deleteConcert")
	 */
	public function deleteConcertAction(Request $request)
	{
		$concert = new Concert();

		$form = $this->createFormBuilder($concert)
			->add('nomConcert', TextType::class)
			->add('delete', SubmitType::class,array('label' => 'Delete'))
			->getForm();

		$form->handleRequest($request);

		if($form->isSubmitted() && $form->isValid()){
			$em = $this->getDoctrine()->getManager();
			$concert = $em->getRepository('AppBundle:Concert')
					->findOneBy($concert->getNomConcert());

			if(!$concert){
				return $this->render('default/message.html.twig',
					array('message' => 'No concerts found for ' . $concert->getNomConcert()));
			}
			$id = $concert->getCodi();
			$em->remove($concert);
			$em->flush();
			return $this->render('default/message.html.twig',
				array('message' => 'Concert deleted: ' . $id));
		}
		return $this->render('default/form.html.twig',
			array('title' => 'Delete Concert',
				'form' => $form->createView(),
		));
	}

}
