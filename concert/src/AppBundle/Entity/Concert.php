<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Concert
 *
 * @ORM\Table(name="concert", uniqueConstraints={@ORM\UniqueConstraint(name="nomConcert", columns={"nomConcert"})})
 * @ORM\Entity
 */
class Concert
{
    /**
     * @var string
     *
     * @ORM\Column(name="nomConcert", type="string", length=250, nullable=true)
     */
    private $nomconcert;

    /**
     * @var string
     *
     * @ORM\Column(name="nomGrup", type="string", length=150, nullable=true)
     */
    private $nomgrup;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data", type="date", nullable=true)
     */
    private $data;

    /**
     * @var string
     *
     * @ORM\Column(name="ciutat", type="string", length=50, nullable=true)
     */
    private $ciutat;

    /**
     * @var string
     *
     * @ORM\Column(name="espai", type="string", length=50, nullable=true)
     */
    private $espai;

    /**
     * @var boolean
     *
     * @ORM\Column(name="codi", type="boolean")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $codi;



    /**
     * Set nomconcert
     *
     * @param string $nomconcert
     *
     * @return Concert
     */
    public function setNomconcert($nomconcert)
    {
        $this->nomconcert = $nomconcert;

        return $this;
    }

    /**
     * Get nomconcert
     *
     * @return string
     */
    public function getNomconcert()
    {
        return $this->nomconcert;
    }

    /**
     * Set nomgrup
     *
     * @param string $nomgrup
     *
     * @return Concert
     */
    public function setNomgrup($nomgrup)
    {
        $this->nomgrup = $nomgrup;

        return $this;
    }

    /**
     * Get nomgrup
     *
     * @return string
     */
    public function getNomgrup()
    {
        return $this->nomgrup;
    }

    /**
     * Set data
     *
     * @param \DateTime $data
     *
     * @return Concert
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set ciutat
     *
     * @param string $ciutat
     *
     * @return Concert
     */
    public function setCiutat($ciutat)
    {
        $this->ciutat = $ciutat;

        return $this;
    }

    /**
     * Get ciutat
     *
     * @return string
     */
    public function getCiutat()
    {
        return $this->ciutat;
    }

    /**
     * Set espai
     *
     * @param string $espai
     *
     * @return Concert
     */
    public function setEspai($espai)
    {
        $this->espai = $espai;

        return $this;
    }

    /**
     * Get espai
     *
     * @return string
     */
    public function getEspai()
    {
        return $this->espai;
    }

    /**
     * Get codi
     *
     * @return boolean
     */
    public function getCodi()
    {
        return $this->codi;
    }
}
