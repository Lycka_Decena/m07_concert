<?php

/* concert/content.html.twig */
class __TwigTemplate_f4b6d5fb7fbae2f795d7db82a43318058d5a2289c55a6cbdd6e2527f3a9cbaaa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "concert/content.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "concert/content.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "concert/content.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "\t<h3> Concert List </h3>
<table border=\"1\">
\t<tr>
\t\t<th>ID</th>
\t\t<th>Name</th>
\t\t<th>Author</th>
\t\t<th>Date</th>
\t\t<th>City</th>
\t\t<th>Pavilion</th>
\t</tr>
\t";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["concerts"] ?? $this->getContext($context, "concerts")));
        foreach ($context['_seq'] as $context["_key"] => $context["concert"]) {
            // line 15
            echo "\t\t<tr>
\t\t\t<td>";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($context["concert"], "codi", array()), "html", null, true);
            echo " </td>
\t\t\t<td>";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["concert"], "nomConcert", array()), "html", null, true);
            echo " </td>
\t\t\t<td>";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($context["concert"], "nomGrup", array()), "html", null, true);
            echo " </td>
\t\t\t<td>";
            // line 19
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["concert"], "data", array()), "d-m-Y"), "html", null, true);
            echo " </td>
\t\t\t<td>";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute($context["concert"], "ciutat", array()), "html", null, true);
            echo " </td>
\t\t\t<td>";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($context["concert"], "espai", array()), "html", null, true);
            echo " </td>
\t\t</tr>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['concert'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "</table>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "concert/content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 24,  88 => 21,  84 => 20,  80 => 19,  76 => 18,  72 => 17,  68 => 16,  65 => 15,  61 => 14,  49 => 4,  40 => 3,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# app/Resources/views/concert/content.html.twig #}
{% extends 'base.html.twig' %}
{% block body %}
\t<h3> Concert List </h3>
<table border=\"1\">
\t<tr>
\t\t<th>ID</th>
\t\t<th>Name</th>
\t\t<th>Author</th>
\t\t<th>Date</th>
\t\t<th>City</th>
\t\t<th>Pavilion</th>
\t</tr>
\t{% for concert in concerts %}
\t\t<tr>
\t\t\t<td>{{ concert.codi }} </td>
\t\t\t<td>{{ concert.nomConcert }} </td>
\t\t\t<td>{{ concert.nomGrup }} </td>
\t\t\t<td>{{ concert.data | date('d-m-Y') }} </td>
\t\t\t<td>{{ concert.ciutat }} </td>
\t\t\t<td>{{ concert.espai }} </td>
\t\t</tr>
\t{% endfor %}
</table>


{% endblock %}
", "concert/content.html.twig", "/home/ausias/symfony/concert/app/Resources/views/concert/content.html.twig");
    }
}
